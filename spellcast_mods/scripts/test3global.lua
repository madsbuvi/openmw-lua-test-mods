local I = require('openmw.interfaces')
local core = require('openmw.core')
local types = require('openmw.types')
local world = require('openmw.world')
local util = require('openmw.util')
local Weapon = types.Weapon
local NPC = types.NPC
local Actor = types.Actor
local Creature = types.Creature
local Item = types.Item

local function onMagicBoltHit(data)
    local result, projectile = data.result, data.projectile
    print('Bounce: '..tostring(projectile.options.bouncy))

    -- Don't bounce off of legitimate targets
    if result.hit then
        print('hit: '..tostring(result.hitObject))
        return true
    end

    if not projectile.options.bouncy then return true end

    local direction, speed = result.hitVelocity:normalize()
    local normal = result.hitNormal:normalize()
    local cosa = math.abs(normal * direction)

    -- The faster we're going, the less it takes to bounce
    -- These numbers are completely arbitrary
    if speed > 400 and cosa <=  math.max(speed / 2000, 0.50) then
        -- reflect
        speed = speed * math.min(0.85, (1 - cosa))
        local reflect = (direction + (normal * cosa * 2)):normalize()

        projectile.options.speed = speed
            -- Pre-add some movement to prevent projectile from making an instant impact with the ground
        projectile.origin = result.hitPos + reflect
        projectile.orientation = util.transform.makeRotate(util.vector3(0, 1, 0), reflect)
        core.sendGlobalEvent('LaunchProjectile', projectile)

        return false
    end
    return true
end

return {
    eventHandlers = {
        MagicBoltHit = onMagicBoltHit
    }
}
