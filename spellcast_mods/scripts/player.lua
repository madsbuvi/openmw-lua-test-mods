local ui = require('openmw.ui')
local I = require('openmw.interfaces')
local core = require('openmw.core')
local animation = require('openmw.animation')
local types = require('openmw.types')
local input = require('openmw.input')
local async = require('openmw.async')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local Weapon = types.Weapon
local Player = types.Player
local NPC = types.NPC
local Actor = types.Actor
local Creature = types.Creature
local Item = types.Item
local Spell = I.Classes.Spell

-- Test 1: Casting a spell normally ( + some ctrl/shift/alt modifiers)
local test1 = require('scripts.test1')

-- Test 2: Inflict a spell (paralysis) on a target ( + some ctrl/shift/alt modifiers)
local test2 = require('scripts.test2')

-- Test 3: Make destruction spells affected by gravity, and change the casting sequence animations to allow a charge-up effect
local test3 = require('scripts.test3')

-- Test 4: Override spell class to replace % chance with dynamic casting cost
local test4Enabled = false
local test4 = {func = function()
    test4Enabled = not test4Enabled
    self:sendEvent('Test4Enabled', test4Enabled)
end}

local bindings = {}

local function bindTestFunct(binding)
    bindings[#bindings+1] = binding
end
bindTestFunct({ callback = test1.func, key = input.KEY.Delete, triggerValue = true })
bindTestFunct({ callback = test2.func, key = input.KEY.Insert, triggerValue = true })
bindTestFunct({ callback = test3.func, key = input.KEY.End, triggerValue = true })
bindTestFunct({ callback = test4.func, key = input.KEY.Home, triggerValue = true })

local function init()
    ui.showMessage('TestScript active')
end

local initialized = false
local function onUpdate(dt)
    if not initialized then
        initialized = true
        init()
    end

    for _, binding in pairs(bindings) do
        local value = input.isKeyPressed(binding.key)
        if value ~= binding.value then
            if not binding.triggerValue or value == binding.triggerValue then
                binding.callback()
            end
            binding.value = value
        end
    end
end

return {
    engineHandlers = {
        onUpdate = onUpdate,
    },
}
