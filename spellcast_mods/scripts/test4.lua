local ui = require('openmw.ui')
local I = require('openmw.interfaces')
local core = require('openmw.core')
local types = require('openmw.types')
local self = require('openmw.self')
local NPC = types.NPC
local Actor = types.Actor
local Creature = types.Creature

local BaseSpell = I.Classes.Spell
local DynamicSpell = BaseSpell:new()
DynamicSpell.__index = DynamicSpell

local enabled = false

function DynamicSpell:getChance(actor)
    if enabled then
        return nil
    end
    print('chance overridden')
    return BaseSpell.getChance(self, actor)
end

function DynamicSpell:getCostMagicka(actor)
    local baseCost = BaseSpell.getCostMagicka(self, actor)
    if not enabled then
        return baseCost
    end
    print('getCostMagicka overridden')

    if not baseCost or baseCost <= 1 then
        return baseCost
    end

    local skillId = self:getSchool(actor)
    local skillLevel = 5
    if NPC.objectIsInstance(actor) then
        skillLevel = NPC.stats.skills[skillId](actor).modified
    elseif Creature.objectIsInstance(actor) then
        specialization = core.stats.Skill.record(skillId).specialization
        local creatureRecord = Creature.record(actor)
        skillLevel = creatureRecord[specialization..'Skill']
    end

    local factor = math.sqrt(baseCost) * 2
    if factor > skillLevel then
        -- Massively increase the cost if skill is much too low to cast a spell
        factor = 0.1 + (skillLevel / factor) * 0.9
    else
        factor = skillLevel / factor
    end
    local cost = math.max(1, baseCost / factor)

    print('cost overridden: '..tostring(baseCost)..' -> '..tostring(cost))
    return cost
end

local interface = {}
for k, v in pairs(I.Classes) do
    interface[k] = v
end
interface.Spell = DynamicSpell

return {
    interfaceName = 'Classes',
    interface = interface,
    eventHandlers = {
        Test4Enabled = function(data)
            if data then
                -- Enables dynamic spell costs by overriding the spell class with
                -- custom getChance and getCostMagicka
                -- NOTE: At the current state of dehardcoding UI, custom costs and chances will not
                -- be reflected in the UI.
                ui.showMessage('Running test 4: Enable dynamic spellcosts')
                enabled = true
            else
                ui.showMessage('Test 4: Disabling dynamic spellcosts')
                enabled = false
            end
        end
    }
}
