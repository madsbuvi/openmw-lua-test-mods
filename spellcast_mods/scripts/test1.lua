local ui = require('openmw.ui')
local I = require('openmw.interfaces')
local core = require('openmw.core')
local input = require('openmw.input')
local self = require('openmw.self')
local nearby = require('openmw.nearby')

local function findNPC(recordId)
    for _, actor in pairs(nearby.actors) do
        if actor.recordId == recordId then return actor end
    end
    ui.showMessage('Could not find actor "'..tostring(recordId)..'" nearby.')
    return nil
end

-- Test 1: Casting a spell using regular animation sequences.
return {
    func = function()
        ui.showMessage('Running test 1: Cast fireball')
        local spellcast = I.Classes.Spellcast:create('Fireball')

        -- If user is pressing shift, cast instantly without animation
        if not input.isShiftPressed() then
            spellcast:enableAnimation()
        end
        -- If user is pressing ctrl, make the spell affected by gravity
        if input.isCtrlPressed() then
            spellcast.projectile.speed = 1000
            spellcast.projectile.gravityMultiplier = 0.9
        end
        -- If user is pressing alt, force target fargoth.
        if input.isAltPressed() then
            print('Targetting fargoth')
            spellcast.target = findNPC('fargoth')
        end

        I.ActorController.playAction(spellcast)
    end
}
