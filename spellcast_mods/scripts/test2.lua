local ui = require('openmw.ui')
local I = require('openmw.interfaces')
local core = require('openmw.core')
local animation = require('openmw.animation')
local types = require('openmw.types')
local input = require('openmw.input')
local async = require('openmw.async')
local nearby = require('openmw.nearby')
local self = require('openmw.self')
local Weapon = types.Weapon
local Player = types.Player
local NPC = types.NPC
local Actor = types.Actor
local Creature = types.Creature
local Item = types.Item


local function findNPC(recordId)
    for _, actor in pairs(nearby.actors) do
        if actor.recordId == recordId then return actor end
    end
    ui.showMessage('Could not find actor "'..tostring(recordId)..'" nearby.')
    return nil
end

-- Test2 Directly afflict a specific NPC (Fargoth) with a spell. Optionally, a caster can be assigned to cause a crime.
return {
    func = function()
        ui.showMessage('Running test 2: Paralyze fargoth')
        local fargoth = findNPC('fargoth')
        if fargoth then
            local spell = I.Classes.Spell:create('Paralysis')
            local params = {
                spell = spell:save(),
                target = fargoth,
                options = {
                    caster = selfObj,
                    ignoreResistances = self.godMode or self.ignoreResistances,
                    ignoreSpellAbsorption = self.godMode or self.ignoreSpellAbsorption,
                    ignoreReflect = self.godMode or self.ignoreReflect,
                },
            }
            if input.isShiftPressed() then
            -- Assign a caster to aggro the target and cause a crime.
                params.options.caster = self
            end
            core.sendGlobalEvent('Inflict', params)
        end
    end
}
