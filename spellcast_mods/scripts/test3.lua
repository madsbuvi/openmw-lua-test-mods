local ui = require('openmw.ui')
local I = require('openmw.interfaces')
local core = require('openmw.core')
local animation = require('openmw.animation')
local types = require('openmw.types')
local input = require('openmw.input')
local async = require('openmw.async')
local nearby = require('openmw.nearby')
local selfObj = require('openmw.self')
local Weapon = types.Weapon
local Player = types.Player
local NPC = types.NPC
local Actor = types.Actor
local Creature = types.Creature
local Item = types.Item

local drawAnimation = nil
local releaseAnimation = nil
local followAnimation = nil

local function beginDraw()
    drawAnimation = I.Classes.Animation:create('throwweapon', {
        startKey = 'shoot start',
        stopKey = 'shoot max attack',
        speed = 0.33,
        priority = {
            [animation.BONE_GROUP.RightArm] = animation.PRIORITY.Weapon,
            [animation.BONE_GROUP.LeftArm] = animation.PRIORITY.Weapon,
            [animation.BONE_GROUP.Torso] = animation.PRIORITY.Weapon,
            [animation.BONE_GROUP.LowerBody] = animation.PRIORITY.WeaponLowerBody
        },
        autoDisable = false,
    })
    drawAnimation:play()
end

local function beginRelease(completion)
    releaseAnimation = I.Classes.Animation:create('throwweapon', {
        startKey = 'shoot max attack',
        stopKey = 'shoot release',
        speed = 0.66,
        priority = {
            [animation.BONE_GROUP.RightArm] = animation.PRIORITY.Weapon,
            [animation.BONE_GROUP.LeftArm] = animation.PRIORITY.Weapon,
            [animation.BONE_GROUP.Torso] = animation.PRIORITY.Weapon,
            [animation.BONE_GROUP.LowerBody] = animation.PRIORITY.WeaponLowerBody
        },
    })
    local minHit = releaseAnimation:getTextKeyTimeRelative('shoot min hit')
    releaseAnimation.startpoint = (1 - completion) * minHit
    releaseAnimation:play()
end

local function beginFollow()
    followAnimation = I.Classes.Animation:create('throwweapon', {
        startKey = 'shoot follow start',
        stopKey = 'shoot follow stop',
        speed = 0.66,
        priority = {
            [animation.BONE_GROUP.RightArm] = animation.PRIORITY.Weapon,
            [animation.BONE_GROUP.LeftArm] = animation.PRIORITY.Weapon,
            [animation.BONE_GROUP.Torso] = animation.PRIORITY.Weapon,
            [animation.BONE_GROUP.LowerBody] = animation.PRIORITY.WeaponLowerBody
        },
    })
    followAnimation:play()
end

local function test3Handler(cast)
    if not cast.projectile or cast.spell:getSchool(selfObj) ~= 'destruction' then
        -- Return true to allow the regular cast sequence to play out
        return true
    end

    function cast:update(dt)
        local use = input.getBooleanActionValue('Use') and 1 or 0
        if followAnimation then
            local completion = followAnimation:getCompletion()
            if completion == nil or completion >= 1 then
                print('follow done')
                followAnimation:cancel()
                followAnimation = nil
                self:complete()
                return true
            end
            return false
        end

        if releaseAnimation then
            local completion = releaseAnimation:getCompletion()
            if completion == nil or completion >= 1 then
                releaseAnimation:cancel()
                releaseAnimation = nil
                beginFollow()
                self:release()
            end
            return false
        end

        if drawAnimation then
            local min = drawAnimation:getTextKeyTime('shoot min attack')
            local max = drawAnimation:getTextKeyTime('shoot max attack')
            local completion = (drawAnimation:getCurrentTime() - min) / (max - min)
            if use == 0 and completion >= 0 then
                self.projectile.speed = completion * 1000 + 500
                print('speed: '..tostring(self.projectile.speed))
                self.projectile.gravityMultiplier = 0.48
                print('gravity: '..tostring(self.projectile.gravityMultiplier))
                drawAnimation:cancel()
                drawAnimation = nil
                beginRelease(completion)
            end
            return false
        end
        return true
    end

    -- Note that since we are implementing our own play/update, we have to make
    -- :start(), :release(), and :complete() calls ourselves as part of these.
    -- :start() sets the actor as busy, and applies casting costs, :release() causes effects to be
    -- applied and the projectile to be launched, and :complete() undoes the busy state.

    function cast:play()
        if self:start() then
            beginDraw()
            self.spell:playCastingEffects(selfObj)
            I.SpellCasting.playMagicEffects('cast', self.spell:getEffects())
            return true
        end
        return false
    end

    if input.isShiftPressed() then
        -- Make the projectile bouncy
        cast.projectile.bouncy = true
    end

    return true
end
local test3HandlerAdded = false
-- Test 3: Make destruction spells affected by gravity, and change the casting sequence animations to allow a charge-up effect
return {
    func = function()
        ui.showMessage('Running test 3: Enable gravity spells')
        if not test3HandlerAdded then
            I.SpellCasting.addOnSpellCastHandler(test3Handler)
            test3HandlerAdded = true
        end
    end
}
